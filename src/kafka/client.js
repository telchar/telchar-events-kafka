const { KafkaClient } = require('kafka-node');
const config = require('../config');
module.exports = new KafkaClient({
  kafkaHost: config.HOST,
});
