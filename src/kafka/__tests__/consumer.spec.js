jest.mock('../../config', () => ({
  HOST: 'fake:port',
  EVENT_GROUP: 'fake_group',
  EVENT_TOPIC: 'fake_topic',
}));
jest.mock('../client', () => jest.mock());
jest.mock('kafka-node', () => {
  const handlers = {};

  const simulate = (event, ...args) => {
    (handlers[event] || []).forEach(h => h(...args));
  };

  const offset = {
    fetch: jest.fn(),
  };

  const instance = {
    addTopics: jest.fn(),
    setOffset: jest.fn(),
    on: jest.fn().mockImplementation((event, handler) => {
      if (handlers[event]) {
        handlers[event].push(handler);
      } else {
        handlers[event] = [handler];
      }
    }),
  };
  return {
    simulate,
    instance,
    offset,
    Consumer: jest.fn().mockImplementation(() => (instance)),
    Offset: jest.fn().mockImplementation(() => offset),
  };
});

const client = require('../client');
const {
  Consumer, instance, offset, simulate,
} = require('kafka-node');
const consumer = require('../consumer');

describe('Kafka consumer', () => {
  beforeEach(() => {
    jest.resetModules();
    instance.on.mockClear();
    instance.addTopics.mockClear();
    offset.fetch.mockClear();
    instance.setOffset.mockClear();
  });

  it('should create with options', () => {
    const handler = jest.fn();
    consumer.init(handler);
    const args = Consumer.mock.calls[0];
    expect(args[0]).toBe(client);
    expect(args[1]).toEqual([{ topic: 'fake_topic' }]);
    expect(args[2]).toEqual({ groupId: 'fake_group' });
    expect(instance.on).toHaveBeenCalledWith('message', handler);
  });

  it('should handle error', () => {
    simulate('error', new Error('fake_error'));
  });

  it('should adjust offset', () => {
    const offsets = { fake_event: { 0: [10, 11, 12, 13] } };
    offset.fetch.mockImplementation((topic, cb) => cb(null, offsets));
    const topic = { topic: 'fake_event', partition: 0 };
    simulate('offsetOutOfRange', topic);
    const args = offset.fetch.mock.calls[0];
    expect(offset.fetch).toHaveBeenCalled();
    expect(args[0]).toEqual([topic]);
    expect(instance.setOffset).toHaveBeenCalledWith(
      'fake_event', 0, 10,
    );
  });

  it('should handle adjust offset error', () => {
    offset.fetch.mockImplementation((topic, cb) => cb(new Error('Fake error')));
    const topic = { topic: 'fake_event', partition: 0 };
    simulate('offsetOutOfRange', topic);
    const args = offset.fetch.mock.calls[0];
    expect(offset.fetch).toHaveBeenCalled();
    expect(args[0]).toEqual([topic]);
    expect(instance.setOffset).not.toHaveBeenCalled();
  });
});
