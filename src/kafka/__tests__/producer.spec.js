jest.mock('../../config', () => ({
  HOST: 'fake:port',
  EVENT_GROUP: 'fake_group',
  EVENT_TOPIC: 'fake_topic',
}));
jest.mock('../client', () => jest.mock());
jest.mock('kafka-node', () => {
  const handlers = {};

  const simulate = (event) => {
    (handlers[event] || []).forEach(h => h());
  };

  const instance = {
    connect: jest.fn(),
    send: jest.fn().mockImplementation((msg, cb) => { cb(); }),
    on: jest.fn().mockImplementation((event, handler) => {
      if (handlers[event]) {
        handlers[event].push(handler);
      } else {
        handlers[event] = [handler];
      }
    }),
  };
  return {
    simulate,
    instance,
    Producer: jest.fn().mockImplementation(() => (instance)),
  };
});

const Kafka = require('kafka-node');
const producer = require('../producer');

describe('Kafka Producer', () => {
  beforeEach(() => {
    Kafka.instance.send.mockClear();
  });
  it('should not produce messages without init', () => {
    try {
      producer.send('topic', 'message');
      expect(false).toBeTruthy();
    } catch (e) {
      expect(e.message).toBe('Producer not ready...');
    }
  });

  it('should init', () => {
    producer.init();
    expect(Kafka.Producer).toHaveBeenCalled();
  });

  it('should produce messages', () => {
    producer.send('topic', 'message');
    const args = Kafka.instance.send.mock.calls[0];
    expect(args[0]).toEqual([
      { topic: 'topic', messages: ['message'] },
    ]);
  });

  it('should produce messages as JSON', () => {
    producer.sendJSON('topic', { message: 'teste' });
    const args = Kafka.instance.send.mock.calls[0];
    expect(args[0]).toEqual([
      { topic: 'topic', messages: ['{"message":"teste"}'] },
    ]);
  });

  it('should handle error', () => {
    Kafka.simulate('event.error', new Error('fake_error'));
  });
});
