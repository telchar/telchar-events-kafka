const client = require('./client');
const { Producer } = require('kafka-node');
let producer = null;

const init = () => {
  producer = new Producer(client);
};

const send = (topic, message) => {
  if (!producer) {
    throw new Error('Producer not ready...');
  }
  producer.send([{
    topic,
    messages: [message],
  }], (error, data) => {
    console.log(
      'produced:', error, data, message,
    );
  });
};

const sendJSON = (topic, message) => {
  send(topic, JSON.stringify(message));
};

module.exports = { init, send, sendJSON };
