const { Consumer, Offset } = require('kafka-node');
const config = require('../config');
const client = require('./client');
let consumer = null;

const init = (handler) => {
  consumer = new Consumer(
    client, [{ topic: config.EVENT_TOPIC }], { groupId: config.EVENT_GROUP },
  );
  const offset = new Offset(client);

  consumer.on('error', (err) => {
    console.error(`Consumer Error: ${config.HOST} `);
    console.error(err);
  });

  consumer.on('message', handler);
  consumer.on('offsetOutOfRange', (topic) => {
    console.error(`Adjusting offset: ${config.HOST} ${JSON.stringify(topic)} `);
    offset.fetch([topic], (err, offsets) => {
      if (err) {
        console.error(err);
        return;
      }
      const min = Math.min.apply(null, offsets[topic.topic][topic.partition]);
      consumer.setOffset(
        topic.topic, topic.partition, min,
      );
    });
  });
};


module.exports = { init };
