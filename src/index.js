const { EVENT_TOPIC } = require('./config');

const consumer = require('./kafka/consumer');
const EventConsumer = require('./event/consumer');

const producer = require('./kafka/producer');

/**
 * Configure kafka consumer
 */
const eventConsumer = new EventConsumer();

module.exports = {
  initConsumer: () => {
    consumer.init((data) => {
      console.log('received message:', data);
      if (data.topic !== EVENT_TOPIC) {
        return;
      }
      eventConsumer.handle(JSON.parse(data.value.toString()));
    });
  },
  initProducer: () => {
    producer.init();
  },
  setHandler: (event, handlers) => {
    eventConsumer.setHandler(event, handlers);
  },
  setErrorHandler: (handler) => {
    eventConsumer.setErrorHandler(handler);
  },
  send(name, payload) {
    try {
      const event = { name, payload };
      producer.sendJSON(EVENT_TOPIC, event);
    } catch (err) {
      console.error('A problem occurred when sending event');
      console.error(err);
    }
  },
};
