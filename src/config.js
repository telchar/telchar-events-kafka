require('require-environment-variables')(['KAFKA_HOST', 'KAFKA_EVENT_GROUP']);

module.exports = {
  HOST: process.env.KAFKA_HOST,
  EVENT_GROUP: process.env.KAFKA_EVENT_GROUP,
  EVENT_TOPIC: process.env.KAFKA_EVENT_TOPIC || 'events',
};

