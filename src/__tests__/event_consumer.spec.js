jest.mock('../kafka/consumer', () => {
  const handlers = [];
  const simulate = (data) => {
    handlers.forEach(h => h(data));
  };
  return {
    init: jest.fn().mockImplementation((handler) => {
      handlers.push(handler);
    }),
    simulate,
    subscribe: jest.fn(),
    data: (handler) => {
      handlers.push(handler);
    },
  };
});
jest.mock('../config');
jest.mock('../event/consumer', () => {
  const instance = {
    setHandler: jest.fn(),
    setErrorHandler: jest.fn(),
    handle: jest.fn(),
  };
  const classDef = jest.fn().mockImplementation(() => instance);
  classDef.instance = instance;
  return classDef;
});

const kafkaEvents = require('../index');
const kafkaConsumer = require('../kafka/consumer');
const EventConsumer = require('../event/consumer');

describe('Event Consumer', () => {
  beforeEach(() => {
    EventConsumer.instance.setHandler.mockClear();
    EventConsumer.instance.handle.mockClear();
    EventConsumer.instance.setErrorHandler.mockClear();
  });

  it('should init', () => {
    kafkaEvents.initConsumer();
    expect(kafkaConsumer.init).toHaveBeenCalled();
  });

  it('should add handlers', () => {
    const handler = jest.fn();
    kafkaEvents.setHandler('event:teste', [handler]);
    expect(EventConsumer.instance.setHandler).toHaveBeenCalledWith('event:teste', [handler]);
  });

  it('should add composed handler', () => {
    const handler = jest.fn();
    const preHandler = jest.fn();
    kafkaEvents.setHandler('event:teste', [preHandler, handler]);
    expect(EventConsumer.instance.setHandler).toHaveBeenCalledWith('event:teste', [preHandler, handler]);
  });

  it('should execute handle', () => {
    kafkaConsumer.simulate({
      topic: 'events',
      value: Buffer.from('{"name":"event:teste"}'),
    });

    expect(EventConsumer.instance.handle).toHaveBeenCalledWith({ name: 'event:teste' });
  });

  it('should not execute a handlers from another topic', () => {
    kafkaConsumer.simulate({
      topic: 'ANOTHER_TOPIC',
      value: Buffer.from('{"name":"event:unknow"}'),
    });

    expect(EventConsumer.instance.handle).not.toHaveBeenCalled();
  });

  it('should set handler error', () => {
    const handler = jest.fn();
    kafkaEvents.setErrorHandler(handler);
    expect(EventConsumer.instance.setErrorHandler).toHaveBeenCalledWith(handler);
  });
});
