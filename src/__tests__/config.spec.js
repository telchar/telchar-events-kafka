describe('Configuration', () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it('should validate missing variables', () => {
    try {
      require('../config');
      expect(false).toBeTruthy();
    } catch (e) {
      expect(e.message).toBe('Environment variables missing');
    }
  });

  it('should validate with required variables', () => {
    try {
      process.env.KAFKA_HOST = 'fake_host';
      process.env.KAFKA_EVENT_GROUP = 'fake_group';
      const config = require('../config');
      expect(config.HOST).toBe('fake_host');
      expect(config.EVENT_GROUP).toBe('fake_group');
      expect(config.EVENT_TOPIC).toBe('events');
    } catch (e) {
      expect(false).toBeTruthy();
    }
  });

  it('should validate with all variables', () => {
    try {
      process.env.KAFKA_HOST = 'another_fake_host';
      process.env.KAFKA_EVENT_GROUP = 'another_group';
      process.env.KAFKA_EVENT_TOPIC = 'another_topic';
      const config = require('../config');
      expect(config.HOST).toBe('another_fake_host');
      expect(config.EVENT_GROUP).toBe('another_group');
      expect(config.EVENT_TOPIC).toBe('another_topic');
    } catch (e) {
      expect(false).toBeTruthy();
    }
  });
});
