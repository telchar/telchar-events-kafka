jest.mock('../config');
jest.mock('../kafka/producer', () => ({
  send: jest.fn(),
  sendJSON: jest.fn(),
  init: jest.fn(),
}));

const producer = require('../index');
const kafkaProducer = require('../kafka/producer');

describe('Event Producer', () => {
  beforeAll(() => {
  });

  beforeEach(() => {
    kafkaProducer.send.mockClear();
    kafkaProducer.sendJSON.mockClear();
  });

  it('should init', () => {
    producer.initProducer();
    expect(kafkaProducer.init).toHaveBeenCalled();
  });

  it('should send event', () => {
    producer.send('event:teste', { message: 'opa' });
    expect(kafkaProducer.sendJSON).toHaveBeenCalled();
    expect(kafkaProducer.sendJSON).toHaveBeenCalledWith('events', {
      name: 'event:teste',
      payload: { message: 'opa' },
    });
  });

  it('should handle event error', () => {
    kafkaProducer.sendJSON.mockImplementation(() => { throw new Error('fake error'); });
    producer.send('event:teste', { message: 'opa' });
  });
});
