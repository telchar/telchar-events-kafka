class EventConsumer {
  constructor() {
    this.handlers = {};
    this.errorHandler = (error) => {
      console.log('Error found: ', error);
    };
  }

  setHandler(event, handlers) {
    if (Array.isArray(handlers)) {
      this.handlers[event] = handlers;
    } else {
      this.handlers[event] = [handlers];
    }
  }

  setErrorHandler(handler) {
    this.errorHandler = handler;
  }

  handle(event) {
    const eventHandlers = this.handlers[event.name] || [];

    if (eventHandlers.length === 0) {
      console.warn(`Event ${event.name} has no handler.`);
      return;
    }

    try {
      eventHandlers.forEach((h) => {
        h(event);
      });
    } catch (error) {
      this.errorHandler({
        name: `${event.name}:error`,
        message: { error },
      });
    }
  }
}

module.exports = EventConsumer;
