const EventConsumer = require('../consumer');
const consumer = new EventConsumer();

describe('Event Consumer', () => {
  const handler = jest.fn();
  const anotherHandler = jest.fn();
  const preHandler = jest.fn();

  beforeEach(() => {
    handler.mockClear();
    preHandler.mockClear();
    anotherHandler.mockClear();
  });

  it('should add handlers', () => {
    consumer.setHandler('event:teste', handler);
    consumer.handle({ name: 'event:teste', id: '1' });
    expect(handler).toHaveBeenCalledWith({ name: 'event:teste', id: '1' });
  });

  it('should add composed handler', () => {
    consumer.setHandler('event:teste', handler);
    consumer.setHandler('event:teste', [preHandler, anotherHandler]);
    consumer.handle({ name: 'event:teste' });

    expect(preHandler).toHaveBeenCalledWith({ name: 'event:teste' });
    expect(anotherHandler).toHaveBeenCalledWith({ name: 'event:teste' });
    expect(handler).not.toHaveBeenCalled();
  });

  it('should not execute a handlers without handler', () => {
    consumer.setHandler('event:teste', handler);
    consumer.handle({ name: 'event:unexistent' });

    expect(handler).not.toHaveBeenCalled();
  });

  it('should handle handler error', () => {
    consumer.setHandler('event:any', () => {
      throw new Error('Fake error');
    });
    consumer.handle({ name: 'event:any' });
  });

  it('should handle handler error with custom handler', () => {
    const errorHandler = jest.fn();
    consumer.setErrorHandler(errorHandler);
    consumer.setHandler('event:any', () => {
      throw new Error('Fake error');
    });
    consumer.handle({ name: 'event:any' });
    expect(errorHandler).toHaveBeenCalled();
  });
});
