require('dotenv').config();
const { initProducer, send } = require('../src');

initProducer();
setInterval(() => {
  send('message:created', {
    message: 'Opa!',
    time: Date.now(),
  });
}, 2000);
