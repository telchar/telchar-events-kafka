require('dotenv').config();
const { initConsumer, setHandler } = require('../src/index');

const valida = () => {
  console.log('previamente...');
};

initConsumer();
setHandler('message:created', [valida, ({ payload }) => {
  console.log('Recebendo mensagem: ', payload);
}]);
