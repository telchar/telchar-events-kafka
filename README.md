# telchar-events-kafka

Events structure with Kafka

## Deploy

O deploy para o nexus está vinculado com o versionamento da lib.
Para isso basta rodar o comando:

    npm version path

Isso irá fazer o commit, gerar a tag da versão, e atualizar ela no nexus.

A lib ficará acessivel em: https://nexus.painel.rivendel.com.br/#browse/browse:npm-private:telchar-events-kafka

